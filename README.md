bst to lorry
===

## What is this?

A script that takes a BuildStream project, reads through the dependency
elements and outputs a set of `.lorry` files for use in setting up lorry
mirroring.

A mirrors.yml file will also be output containing the necessary alias
mirror paths for inclusion in the BuildStream `project.conf` file.

## How to use it?

Run the script either from the root of a BuildStream project or using
the `-r` parameter to point to the root of a BuildStream project.

The `.lorry` files will be created in a subdirectory of the project, by
default `lorries`. The name can be changed with the `-o` parameter.

To output `mirrors.yml` the `-m` parameter needs to be used. This should
be the URL prefix of the mirroring location that lorry will use. That is
`mirror-base-url-push` in lorry terms. Alternatively, consider that this
value will be used (with an alias specific suffix) in place of the
aliases in BuildStream.

You may want to ignore some existing BuildStream aliases for
dependencies you do not want to mirror (e.g. those you already host).
The `-i` parameter can be followed by a list of aliases to ignore.

## Manual Intervention

- Currently the script will complain about `git_tag` BuildStream sources
with a URL that does not end in `.git`. There is no simple and foolproof
way to programatically resolve a URL into a valid `.git` address so it
is on the user to ensure these are updated and then re-run the script.
- Currently the script does not change the BuildStream `.bst` files it
reads. It will complain if:
  - A source being mirrored does not use an alias (since this is
  required for the way mirroring works in BuildStream).
  - An alias is used by both `tar` and `git_tag` source types. This is
  problematic only for the way we currently mirror tars as raw files
  using Lorry (since the URL to access in individual file includes the
  additional prefix `/-/blob/master/` in GitLab).