#!/usr/bin/env python3

# Must execute with working directory as root of Buildstream project.
# Will read through all .bst files in the elements directory and
# output .lorry files in a 'lorries' directory.

import os
import sys
import shutil
import argparse
import urllib.parse
from typing import Iterable, Tuple
import yaml

# Helper method to ask a y/n (e.g. continue anyway?) question
def confirm(question: str):
    ans = input(f'{question} (y/n)\n').lower().strip()
    while (ans not in ['y', 'n']):
        ans = input(f'{question} (y/n)\n').lower().strip()

    return ans == 'y'

# Helper method to abort execution for some reason
def abort(reason: str) -> None:
    print(f'Aborted: {reason}', file=sys.stderr)
    exit()

def warn(reason: str) -> None:
    print(f'Warning: {reason}')

class BstToLorry:
    # Files to write will be stored here
    # key = .lorry file path
    # value = output yaml dict
    __output = {}

    # BuildStream aliases read to here
    # key = alias
    # value = replacement
    __aliases = {}

    # .bst file currently being processed
    # useful for error messages
    __curFile = ""

    # Tracking aliases used by git and tar sources as they must be exclusive for a valid setup
    # (due to the way lorry raw file mirroring works and the way Buildstream mirroring work)
    __tar_aliases = {}
    __git_aliases = {}

    def __init__(
        self,
        root_dir: str,
        out_dir_name: str,
        mirrors_prefix: str = '',
        ignore_aliases: Iterable = None
    ) -> None:
        self.__root_dir = root_dir
        self.__out_dir = os.path.join(root_dir, out_dir_name)
        self.__mirrors_prefix = mirrors_prefix
        self.__ignore_aliases = [] if ignore_aliases is None else ignore_aliases

        # Preparing mirroring output if prefix specified
        if mirrors_prefix:
            self.__mirrors_file = os.path.join(root_dir, 'mirrors.yml')
            self.__output[self.__mirrors_file] = {
                'mirrors': [{
                    'name': 'lorry_mirrors',
                    'aliases': {}
                }]
            }

        self.__aliases_file = os.path.join(root_dir, 'aliases.yml')
        self.__output[self.__aliases_file] = {}
        self.__tar_aliases_file = os.path.join(root_dir, 'tar_aliases.yml')
        self.__output[self.__tar_aliases_file] = {}


    def read_proj_config(self) -> Tuple[str, dict]:
        conf_path = os.path.join(self.__root_dir, 'project.conf')

        if not os.path.isfile(conf_path):
            abort(f'{conf_path} file not found')

        with open(conf_path, 'r') as conf:
            config = yaml.safe_load(conf)

        # Element path is always a subdirectory
        elements_path = os.path.join(self.__root_dir, config['element-path'])

        if not os.path.isdir(elements_path):
            abort(f'{elements_path} directory not found')

        return elements_path, config['aliases']

    @staticmethod
    def read_element(bst_path):
        with open(bst_path, 'r') as bst:
            cfg = yaml.safe_load(bst)
            if 'sources' in cfg:
                return cfg['sources']
            else:
                return None

    def ignore_url(self, url: str) -> bool:
        for bad_alias in self.__ignore_aliases:
            if url.startswith(bad_alias):
                return True
        return False

    def expand_url(self, url: str) -> str:
        expanded = None
        for alias in self.__aliases:
            if url.startswith(alias):
                # +1 because alias followed by ':'
                url = self.__aliases[alias] + url[len(alias) + 1:]

                # Can never start with more than one
                expanded = alias
                break

        return url, expanded

    def add_alias(self, alias: str, loc: str, is_tar: bool = False) -> None:
        key = self.__tar_aliases_file if is_tar else self.__aliases_file
        self.__output[key][alias] = loc

    def add_mirror(self, alias: str, loc: str) -> None:
        if not self.__mirrors_prefix:
            return
        if not alias:
            return

        # Inject alias mirrors to this dict of lists
        self.__output[self.__mirrors_file]['mirrors'][0]['aliases'] \
        [alias] = [f'{self.__mirrors_prefix}/{loc}/']

    def convert_git(self, src: dict):
        if self.ignore_url(src['url']):
            return

        # Annoyingly not all URLs end with .git
        if src['url'].endswith('.git'):
            key = src['url'].split('/')[-1][:-4]
        else:
            abort(f'Expected .git url, found {src["url"]} in {self.__curFile}')

        # Resolve any BuildStream aliases
        full_url, alias = self.expand_url(src['url'])

        # Output location is constructed to match the URL
        url_components = urllib.parse.urlparse(full_url)

        # Use domain for directory grouping, use full thing
        # Don't want to split and select index because unreliable
        directory = url_components.netloc.replace('.', '_')

        # Can't mirror without an aliased url
        if not alias:
            alias = directory
            warn(f'Need to alias {full_url} in {self.__curFile} for mirror retrieval')
        elif alias not in self.__git_aliases:
            # Track alias use for exclusivity with tar sources
            self.__git_aliases[alias] = self.__curFile

            if alias in self.__tar_aliases:
                warn(f'Alias {alias} is used for both tar and git sources, see {self.__tar_aliases[alias]} and {self.__curFile}')

        self.add_alias(alias, f'{url_components.scheme}://{url_components.netloc}/')
        self.add_mirror(alias, directory)

        out_path = os.path.join(
            self.__out_dir,
            directory,
            f'{directory}.lorry'
        )

        # Output .lorry may already be used by another source
        if out_path not in self.__output:
            self.__output[out_path] = {}

        # Remove preceeding / and trailing .git
        key = url_components.path[1:-4]

        # Don't try to generate refspec from 'track' in .bst
        # Not always a branchname (which refspec expects)
        self.__output[out_path][key] = {
            'type': 'git',
            'url': full_url,
        }

    def convert_tar(self, src: dict):
        if self.ignore_url(src['url']):
            return

        # Resolve any BuildStream aliases
        full_url, alias = self.expand_url(src['url'])

        # Output location is constructed to match the URL
        url_components = urllib.parse.urlparse(full_url)

        # Use domain for directory grouping, use full thing
        # Don't want to split and select index because unreliable
        directory = url_components.netloc.replace('.', '_')

        # Can't mirror without an aliased url
        if not alias:
            # tars require a distinct alias since prefix differs
            alias = f'tar_{directory}'
            warn(f'Need to alias {full_url} in {self.__curFile} for mirror retrieval')
        elif alias not in self.__tar_aliases:
            # Track alias use for exclusivity with tar sources
            self.__tar_aliases[alias] = self.__curFile

            if alias in self.__git_aliases:
                warn(f'Alias {alias} is used for both git and tar sources, see {self.__git_aliases[alias]} and {self.__curFile}')

        # tars are mirrored as git lfs files so prefix includes blob path
        self.add_alias(alias, f'{url_components.scheme}://{url_components.netloc}', True)
        self.add_mirror(alias, f'{directory}/-/blob/master/')

        out_path = os.path.join(
            self.__out_dir,
            directory,
            f'tars_{directory}.lorry'
        )

        # Output .lorry may already be used by another source
        if out_path not in self.__output:
            self.__output[out_path] = {
                'tar-files': {
                    'type': 'raw-file',
                    'urls': {}
                }
            }

        # Remove preceeding / and trailing filename.tar.*
        key = '/'.join(url_components.path.split('/')[1:-1])

        self.__output[out_path]['tar-files']['urls'][key] = full_url

    def prepare_lorries(self, srcs: list[dict]):
        for src in srcs:
            # Check kind first since not all have 'url'
            if src['kind'] == 'git_tag':
                self.convert_git(src)
            elif src['kind'] == 'tar':
                self.convert_tar(src)

    def convert_elements(self, elements_path):
        for root, _, files in os.walk(elements_path):
            for file in files:
                if (file.endswith('.bst')):
                    self.__curFile = os.path.join(root, file)
                    srcs = self.read_element(self.__curFile)

                    if srcs is not None:
                        self.prepare_lorries(srcs)

    def write_output(self):
        for file_path in self.__output:
            # Directory must exist before file can be written
            os.makedirs(os.path.split(file_path)[0], exist_ok=True)

            with open(file_path, 'w') as file:
                yaml.dump(self.__output[file_path], file)

    def run(self):
        if os.path.isdir(self.__out_dir):
            if not confirm(f'Output \'{self.__out_dir}\' directory already exists, replace and continue?'):
                return
            shutil.rmtree(self.__out_dir)
        os.mkdir(self.__out_dir)

        elements_path, self.__aliases = self.read_proj_config()
        self.convert_elements(elements_path)

        self.write_output()

def main(args):
    ignore = args.ignore if args.ignore else []
    root_dir = args.root[0] if args.root else os.getcwd()
    out_dir_name = args.out[0] if args.out else 'lorries'
    mirrors_prefix = args.mirror_loc[0] if args.mirror_loc else ''

    BstToLorry(root_dir, out_dir_name, mirrors_prefix, ignore).run()

_parser = argparse.ArgumentParser(description=
    'Produce a set of .lorry files from a BuildStream project'
)
_parser.add_argument('-r', '--root', nargs=1,
    help='Specify root directory of BuildStream project - '
    'where project.conf resides (default: working directory)',
    required=False
)
_parser.add_argument('-o', '--out', nargs=1,
    help='Specify name of new output subdirectory '
    'to write .lorry files to (default: \'lorries\')',
    required=False
)
_parser.add_argument('-m', '--mirror-loc', nargs=1,
    help='Specify the URL prefix of the mirroring location to produce '
    'a mirrors.yml file that can be included in project.conf',
    required=False
)
_parser.add_argument('-i', '--ignore', nargs='+',
    help='Specify BuildStream aliases you don''t '
    'want to create mirrors for',
    required=False
)

main(_parser.parse_args())
